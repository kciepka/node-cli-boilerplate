const menus = {
    main: `
      node-cli-boilerplate [options] <command>
  
      example .............. example of command
      version ............ show package version
      help ............... show help menu for a command`,

    example: `
      node-cli-boilerplate [options] example 
  
      --option, -o ..... example option`,
}

module.exports = (args, spinner) => {
    const subCmd = args._[0] === 'help'
        ? args._[1]
        : args._[0];

    spinner.stop();
    console.log(menus[subCmd] || menus.main);

}