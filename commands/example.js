module.exports = (args, spinner) => {
  setTimeout(() => {
    spinner.stop();
    console.log(`Example command called successfully with args: ${JSON.stringify(args)}`);
  }, 5000);
}