const { version } = require('../package.json');

module.exports = (args, spinner) => {
    spinner.stop();
    console.log(`v${version}`)
}