const minimist = require('minimist');
const error = require('./utils/errors');
const ora = require('ora');

module.exports = () => {
    const args = minimist(process.argv.slice(2));
    let cmd = args._[0] || 'help';

    if (args.version || args.v) {
        cmd = 'version';
    }

    if (args.help || args.h) {
        cmd = 'help';
    }

    let commands;

    try {
        commands = require("./commands.json");
    }
    catch (ex) {
        return error(`Could not load commands: ${ex}`, true);
    }

    if (!commands[cmd]) return error(`Invalid command: "${cmd}"`, true);

    try {
        let spinner = ora({ text: 'processing...', color: 'white', spinner: 'arc' }).start();
        require(commands[cmd])(args, spinner);

    }
    catch (ex) {
        return error(`Could not execute command: ${ex}`, true);
    }
}